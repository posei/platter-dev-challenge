import { h, render } from 'preact';
import Tabs from './tabs/Tabs';

const tabsRoot = document.getElementById('tabs-root');

if (tabsRoot) {
    const settings = JSON.parse(tabsRoot.dataset.settings || '{}');
    render(<Tabs settings={settings} />, tabsRoot);
}