# Part 1
Added a section of 3 tabs, using compiled tailwind classes. Took about 5 minutes to just get the config working and then building.

# Part 2
This was actually a bit interesting, ts-node complained struggling to find my schema module, so it seems like a node config issue in one of these config files. I understand what is meant to happen so I'll just need to consult StackOverflow a bit to get the module loading issue. Probably about 30 minutes of work here.

# Part
Spend about another 20 minutes with this. Still a bit unfamiliar with Preact and what the generated bundle is actually meant to be doing, so I end up with an unincluded bundle for the moment, but let me just figure one or two things out.