import { build } from 'esbuild';

build({
  entryPoints: ['index.tsx'],
  bundle: true,
  outfile: 'theme/assets/bundle.js',
  loader: { '.tsx': 'tsx' },
  jsxFactory: 'h',
  jsxFragment: 'Fragment',
}).catch(() => process.exit(1));
