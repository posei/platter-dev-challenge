export interface SectionSettings {
    tab1: string;
    tab2: string;
    tab3: string;
    bg_color: string;
    bordered: boolean;
}
  
export const schema = {
    name: "Tabs",
    settings: [{
        type: "text",
        id: "tab1",
        label: "Tab 1 Title",
        default: "Tab content"
      }, {
        type: "text",
        id: "tab2",
        label: "Tab 2 Title",
        default: "Tab content"
      }, {
        type: "text",
        id: "tab3",
        label: "Tab 3 Title",
        default: "Tab content"
      }, {
        type: "color",
        id: "bg_color",
        label: "Background Color",
        default: "#ffffff"
      }, {
        type: "checkbox",
        id: "bordered",
        label: "Bordered",
        default: true
      }]
};  