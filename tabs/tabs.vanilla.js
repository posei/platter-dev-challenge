document.addEventListener("DOMContentLoaded", function() {
    const tab_collection = document.querySelectorAll('.tabs input[type="radio"]');

    // Straightforward way to shift style states to make "tabs".
    tab_collection.forEach(tab => {
        tab.addEventListener('change', function() {
            document.querySelectorAll('.tab-content').forEach(content => content.style.display = 'none');

            this.nextElementSibling.nextElementSibling.style.display = 'block';
        });
    });
});  