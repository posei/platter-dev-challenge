import { schema } from './schema';
import * as fs from 'fs';
import * as path from 'path';

const content = fs.readFileSync(path.resolve(__dirname, 'content.liquid'), 'utf8');
const script = fs.readFileSync(path.resolve(__dirname, 'tabs.vanilla.js'), 'utf8');

const output = `
{% schema %}
    ${JSON.stringify(schema, null, 2)}
{% endschema %}

${content}

<script>
    ${script}
</script>`;

fs.writeFileSync(path.resolve(__dirname, '../sections/tabs.liquid'), output.trim(), 'utf8');
