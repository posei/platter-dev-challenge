import { useState, useEffect } from 'preact/hooks';

interface TabProps {
  settings: {
    tab1: string;
    tab2: string;
    tab3: string;
    bg_color: string;
    bordered: boolean;
  };
}

const Tabs = ({ settings }: TabProps) => {
    const [activeTab, setActiveTab] = useState(0);
    const tabs = [settings.tab1, settings.tab2, settings.tab3];
    const contents = ['This is looks okay so far.', 'This might be ever better.', 'Well, what do you know.'];

    useEffect(() => {
    const tabContent = document.querySelectorAll('.tab-content');

    tabContent.forEach(content => {
        content.style.display = 'none';
    });

    if (tabContent[activeTab]) {
        tabContent[activeTab].style.display = 'block';
    }

    }, [activeTab]);

    return (
        <div className="tabs" style={{ backgroundColor: settings.bg_color }}>
            {tabs.map((tab, index) => (
            <div key={index}>
                <input type="radio" name="tabs" id={`tab${index}`} checked={activeTab === index} onChange={() => setActiveTab(index)} />

                <label for={`tab${index}`} className="cursor-pointer p-2 bg-gray-200 border-b-2">
                {tab}
                </label>

                <div className={`tab-content p-4 border ${settings.bordered ? 'border' : ''}`}>
                    <h2 className="text-xl">{tab}</h2>
                    <p>{contents[index]}</p>
                </div>
            </div>
            ))}
        </div>
    );
};

export default Tabs;
